import PySimpleGUI as sg
import math

sg.ChangeLookAndFeel('Dark')

layout = [
    [sg.Text('Vector Calculator', size=(10,2), font=("Helvetica", 25))],
    [sg.Text('Vector A | X Value', size=(15, 1)), sg.InputText(size=(10,1))],
    [sg.Text('Vector A | Y Value', size=(15, 1)), sg.InputText(size=(10,1))],
    [sg.Text('Vector B | X Value', size=(15, 1)), sg.InputText(size=(10,1))],
    [sg.Text('Vector B | Y Value', size=(15, 1)), sg.InputText(size=(10,1))],
    [sg.Button('Addition'),sg.Button('Subtraction'),sg.Button('Magnitude')],
    [sg.Text('________________', size=(20,1))],
    [sg.Button('3D Vector')],
    ]

window = sg.Window('Vector Calculator', layout)
event, values = window.Read()
print(event, values[0], values[1], values[2], values[3])    # the input data looks like a simple list when auto numbered

if event == "Addition":
    #Retrieve Inputs in GUI
    val1 = values[0] 
    val2 = values[1] 
    val3 = values[2] 
    val4 = values[3] 
    
    #Conevert Inputs into Integers
    val1 = int(val1)
    val2 = int(val2)
    val3 = int(val3)
    val4 = int(val4)

    #Calculate Magnitude & Angle
    magnitudeX = (val1 + val3)**2
    magnitudeY = (val2 + val4)**2
    magnitude = math.sqrt(magnitudeX + magnitudeY)
    angle = math.degrees(math.atan((val2 + val4)/(val1 + val3)))

    #Logic for Adding Angles
    X = val1+val3
    Y = val2+val4
    if X < 0 and Y > 0:
        angle = angle + 180
    elif X < 0 and Y < 0:
        angle = angle + 180
    elif X > 0 and Y < 0:
        angle = angle + 360

    #Print Results
    print(f"\nMAGNITUDE: {magnitude}")
    print(f"ANGLE: {angle}")
    
    layout = [
    [sg.Text('Vector Results', size=(10,2), font=("Helvetica", 25))],
    [sg.Text('Magnitude', size=(8, 1)), sg.Text(magnitude)],
    [sg.Text('Angle', size=(8, 1)), sg.Text(angle)],
    [sg.Graph(canvas_size=(400, 400), graph_bottom_left=(-50, -50), graph_top_right=(50, 50), background_color='black', key='graph', tooltip=None)],
    ]

    window = sg.Window('Vector Calculator', layout, finalize=True)
    
    graph = window['graph'] 
    #Draw Vector A
    point = graph.draw_point((val1,val2), 3, color='white')
    line = graph.draw_line((0, 0), (val1, val2), color='white')

    #Draw Vector B
    point = graph.draw_point((val3,val4), 3, color='white')
    line = graph.draw_line((val1, val2), (val3, val4), color='white')
    
    #Draw Magnitude
    point = graph.draw_point((0,0), 3, color='red')
    line = graph.draw_line((val3, val4), (0, 0), color='red')
    
    #Vector A Text
    Xtext = graph.draw_text(val1,(val1+4,val2), color="white", font=None)
    comma = graph.draw_text(",",(val1+7,val2), color="white", font=None)
    Ytext = graph.draw_text(val2,(val1+10,val2), color="white", font=None)
    #Vector B Text
    Xtext = graph.draw_text(val3,(val3+4,val4), color="white", font=None)
    comma = graph.draw_text(",",(val3+7,val4), color="white", font=None)
    Ytext = graph.draw_text(val4,(val3+10,val4), color="white", font=None)

    #Draw Axis
    xaxis = graph.draw_line((0,-60), (0,60), color='white')
    yaxis = graph.draw_line((-60, 0), (60,0), color='white')
    xaxistext = graph.draw_text("X",(48,-2), color="white", font=None)
    yaxistext = graph.draw_text("Y",(2,48), color="white", font=None)
    
    event, values = window.Read()

if event == "Subtraction":
    #Retrieve Inputs in GUI
    val1 = values[0] 
    val2 = values[1] 
    val3 = values[2] 
    val4 = values[3] 
    
    #Conevert Inputs into Integers
    val1 = int(val1)
    val2 = int(val2)
    val3 = int(val3)
    val4 = int(val4)

    #Calculate Magnitude & Angle
    magnitudeX = (val1 - val3)**2
    magnitudeY = (val2 - val4)**2
    magnitude = math.sqrt(magnitudeX + magnitudeY)
    angle = math.degrees(math.atan((val2 - val4)/(val1 - val3)))

    #Logic for Adding Angles
    X = val1-val3
    Y = val2-val4
    if X < 0 and Y > 0:
        angle = angle + 180
    elif X < 0 and Y < 0:
        angle = angle + 180
    elif X > 0 and Y < 0:
        angle = angle + 360

    #Print Results
    print(f"\nMAGNITUDE: {magnitude}")
    print(f"ANGLE: {angle}")
    
    layout = [
    [sg.Text('Vector Results', size=(10,2), font=("Helvetica", 25))],
    [sg.Text('Magnitude', size=(8, 1)), sg.Text(magnitude)],
    [sg.Text('Angle', size=(8, 1)), sg.Text(angle)],
    [sg.Graph(canvas_size=(400, 400), graph_bottom_left=(-50, -50), graph_top_right=(50, 50), background_color='black', key='graph', tooltip=None)],

    ]

    window = sg.Window('Vector Calculator', layout, finalize=True)
    
    graph = window['graph']
    #Draw Vector A
    point = graph.draw_point((val1,val2), 3, color='white')
    line = graph.draw_line((0, 0), (val1, val2), color='white')

    #Draw Vector B
    point = graph.draw_point((val3,val4), 3, color='white')
    line = graph.draw_line((0, 0), (val3, val4), color='white')

    #Draw Magnitude
    line = graph.draw_line((val1, val2), (val3, val4), color='red')
    
    #Vector A Text
    Xtext = graph.draw_text(val1,(val1+4,val2), color="white", font=None)
    comma = graph.draw_text(",",(val1+7,val2), color="white", font=None)
    Ytext = graph.draw_text(val2,(val1+10,val2), color="white", font=None)
    #Vector B Text
    Xtext = graph.draw_text(val3,(val3+4,val4), color="white", font=None)
    comma = graph.draw_text(",",(val3+7,val4), color="white", font=None)
    Ytext = graph.draw_text(val4,(val3+10,val4), color="white", font=None)

    #Draw Axis
    xaxis = graph.draw_line((0,-60), (0,60), color='white')
    yaxis = graph.draw_line((-60, 0), (60,0), color='white')
    xaxistext = graph.draw_text("X",(48,-2), color="white", font=None)
    yaxistext = graph.draw_text("Y",(2,48), color="white", font=None)

    event, values = window.Read()

if event == "Magnitude":
    val1 = values[0]
    val2 = values[1]
    
    val1 = int(val1)
    val2 = int(val2)

    magnitude = math.sqrt(val1**2 + val2**2)
    angle = math.degrees(math.atan(val2/val1))

    #Logic for Adding Angles
    X = val1
    Y = val2

    if X < 0 and Y > 0:
        angle = angle + 180
    elif X < 0 and Y < 0:
        angle = angle + 180
    elif X > 0 and Y < 0:
        angle = angle + 360

    #Print Results
    print(f"\nMAGNITUDE: {magnitude}")
    print(f"ANGLE: {angle}")
    
    layout = [
    [sg.Text('Vector Results', size=(10,2), font=("Helvetica", 25))],
    [sg.Text('Magnitude', size=(8, 1)), sg.Text(magnitude)],
    [sg.Text('Angle', size=(8, 1)), sg.Text(angle)],
    [sg.Graph(canvas_size=(400, 400), graph_bottom_left=(-50, -50), graph_top_right=(50, 50), background_color='black', key='graph', tooltip=None)],
    ]

    window = sg.Window('Vector Calculator', layout, finalize=True)

    graph = window['graph']
    point = graph.draw_point((X,Y), 3, color='white')
    line = graph.draw_line((0, 0), (X, Y), color='white')
    
    #Draw Axis
    xaxis = graph.draw_line((0,-60), (0,60), color='white')
    yaxis = graph.draw_line((-60, 0), (60,0), color='white')
    xaxistext = graph.draw_text("X",(48,-2), color="white", font=None)
    yaxistext = graph.draw_text("Y",(2,48), color="white", font=None)


    #Text
    Xtext = graph.draw_text(X,(X+4,Y), color="white", font=None) 
    comma = graph.draw_text(",",(X+7,Y), color="white", font=None)
    Ytext = graph.draw_text(Y,(X+10,Y), color="white", font=None)
    
    
    event, values = window.read()
'''
if event == "Graph":
    layout = [[sg.Graph(canvas_size=(400, 400), graph_bottom_left=(-10, -10), graph_top_right=(10, 10), background_color='black', key='graph')],
    [sg.Button('Calculate'),sg.Button('Up'), sg.Button('Down'), sg.Button('Left'), sg.Button('Right')]]

    window = sg.Window('Graph test', layout, finalize=True)

    graph = window['graph']
    circle = graph.draw_circle((0, 0), 3, fill_color='black', line_color='white')
    
    #Draw Axis
    xaxis = graph.draw_line((0,-60), (0,60), color='white')
    yaxis = graph.draw_line((-60, 0), (60,0), color='white')
    xaxistext = graph.draw_text("X",(9.5,-.5), color="white", font=None)
    yaxistext = graph.draw_text("Y",(.5,9.5), color="white", font=None)

    while True:
        event, values = window.read()
        if event is None:
            break
        if event == 'Calculate':
            line = graph.draw_line((0, 0), (X, Y), color='white')
        if event == 'Up':
            graph.MoveFigure(circle, 0, 1)
        elif event == 'Down':
            graph.MoveFigure(circle, 0, -1)
        elif event == 'Right':
            graph.MoveFigure(circle, 1, 0)
        elif event == 'Left':
            graph.MoveFigure(circle, -1, 0)
    
    window.close()
    
    event, values = window.read()
'''

