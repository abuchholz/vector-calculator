import math

qe = input("Would you like to (+) Vectors | (-) Vectors | (Mag) Magnitude of One Vector => ")

if qe == "+":    
    #Input Variables
    ax = input("Input X Value for Vector A ")
    ay = input("Input Y Value for Vector A ")
    bx = input("Input X Value for Vector B ")
    by = input("Input Y Value for Vector B ")
       
    #Convert Strings to Int
    ax = int(ax)
    ay = int(ay)
    bx = int(bx)
    by = int(by)

    #Calculate Magnitude & Angle
    magnitudeX = (ax + bx)**2
    magnitudeY = (ay + by)**2
    magnitude = math.sqrt(magnitudeX + magnitudeY)
    angle = math.degrees(math.atan((ay + by)/(ax+bx)))
        
    #Logic for Adding Angles
    X = ax+bx
    Y = ay+by
    if X < 0 and Y > 0:
        angle = angle + 180
    elif X < 0 and Y < 0:
        angle = angle + 180
    elif X > 0 and Y < 0:
        angle = angle + 360
        
    #Print Results
    print(f"\nMAGNITUDE: {magnitude}")
    print(f"ANGLE: {angle}")

if qe == "-":
    ax = input("Input X Value for Vector A ")
    ay = input("Input Y Value for Vector A ")
    bx = input("Input X Value for Vector B ")
    by = input("Input Y Value for Vector B ")

    #Convert Strings to Int
    ax = int(ax)
    ay = int(ay)
    bx = int(bx)
    by = int(by)

    #Calculate Magnitude & Angle
    magnitudeX = (ax - bx)**2
    magnitudeY = (ay - by)**2
    magnitude = math.sqrt(magnitudeX + magnitudeY)
    angle = math.degrees(math.atan((ay - by)/(ax - bx)))

    #Logic for Adding Angles
    X = ax-bx
    Y = ay-by
    if X < 0 and Y > 0:
        angle = angle + 180
    elif X < 0 and Y < 0:
        angle = angle + 180
    elif X > 0 and Y < 0:
        angle = angle + 360
       
    #Print Results
    print(f"\nMAGNITUDE: {magnitude}")
    print(f"ANGLE: {angle}")

if qe == "Mag":
    ax = input("Input X Value for Vector A ")
    ay = input("Input Y Value for Vecgor A ")

    ax = int(ax)
    ay = int(ay)
    
    magnitude = math.sqrt(ax**2 + ay**2)
    angle = math.degrees(math.atan(ay/ax))

    #Logic for Adding Angles
    X = ax
    Y = ay

    if X < 0 and Y > 0:
        angle = angle + 180
    elif X < 0 and Y < 0:
        angle = angle + 180
    elif X > 0 and Y < 0:
        angle = angle + 360

    #Print Results
    print(f"\nMAGNITUDE: {magnitude}")
    print(f"ANGLE: {angle}")

