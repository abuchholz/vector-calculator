import PySimpleGUI as sg

# Usage of Graph element.

layout = [[sg.Graph(canvas_size=(400, 400), graph_bottom_left=(-10, -10), graph_top_right=(10, 10), background_color='black', key='graph')],
          [sg.Button('Up'), sg.Button('Down'), sg.Button('Left'), sg.Button('Right')]]

window = sg.Window('Graph test', layout, finalize=True)

graph = window['graph']
circle = graph.draw_circle((0, 0), 3, fill_color='black', line_color='white')

while True:
    event, values = window.read()
    if event is None:
        break
    if event == 'Up':
        graph.MoveFigure(circle, 0, 1)
    elif event == 'Down':
        graph.MoveFigure(circle, 0, -1)
    elif event == 'Right':
        graph.MoveFigure(circle, 1, 0)
    elif event == 'Left':
        graph.MoveFigure(circle, -1, 0)

window.close()
